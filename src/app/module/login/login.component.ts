import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RouteConstant } from 'src/app/shared/constants/route.constant';
import { AuthService } from 'src/app/shared/core/auth-guard/auth.service';
import { LoginModel } from './login.model';
import { MatDialog } from '@angular/material/dialog';
import { SimpleModalComponent } from 'src/app/shared/component/simple-modal/simple-modal.component';
import { FormGroup, FormControl } from '@angular/forms';

/**
 *
 *
 * @export
 * @class LoginComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {

  public allowedAccess: boolean;
  public login: LoginModel;
  datemask = [/\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  form: FormGroup = new FormGroup({
    email: new FormControl(''),
    password: new FormControl(''),
    myModel:  new FormControl('')
  });

  /**
   *Creates an instance of LoginComponent.
   * @param {Router} router
   * @param {AuthService} authService
   * @param {MatDialog} dialog
   * @memberof LoginComponent
   */
  constructor(private router: Router, private authService: AuthService, public dialog: MatDialog) {
    this.allowedAccess = this.authService.isRouteAuthenticated();
  }

  /**
   * Check route access
   *
   * @returns {boolean}
   * @memberof LoginComponent
   */
  allowRouteAccess(): boolean {
    this.login = new LoginModel();
    this.login = this.form.value;
    this.authService.setIsAuthenticated(this.login);
    return this.authService.isRouteAuthenticated();
  }

  ngOnInit() {
  }


  /**
   * Navigate to dashboard
   *
   * @memberof LoginComponent
   */
  navigateToDashboard() {
    if (this.allowRouteAccess()) {
      this.router.navigate([RouteConstant.DASHBOARD]);
    } else {
      this.openDialog();
    }
  }


  /**
   * Open popup if invalid credential
   *
   * @memberof LoginComponent
   */
  openDialog() {
    this.dialog.open(SimpleModalComponent);
  }

}

