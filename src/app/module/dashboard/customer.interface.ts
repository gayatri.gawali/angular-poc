export interface ICustomer {
    id: number;
    first_name: string;
    last_name: string;
    email: string;
    gender: string;
    status: boolean;
    billingAddress: string;
}
