import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { RouteConstant } from 'src/app/shared/constants/route.constant';
import { AuthGuardService } from 'src/app/shared/core/auth-guard/auth-guard.service';


const routes: Routes = [
  {
    path: RouteConstant.EMPTY,
    component: DashboardComponent,
    canActivateChild: [AuthGuardService]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
