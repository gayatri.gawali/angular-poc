import { Component, OnInit } from '@angular/core';
import { customerList } from 'src/app/shared/mocked-data/mocked-customer-data';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  columnsToDisplay = ['first_name', 'last_name', 'email', 'gender', 'status'];
  dataSource = customerList;
  detailRowColumns = ['billingAddress', 'status'];
  constructor() { }

  ngOnInit() {
  }

}

