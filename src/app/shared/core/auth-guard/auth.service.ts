import { Injectable } from '@angular/core';
import { LoginModel } from 'src/app/module/login/login.model';
import { UserList } from '../../mocked-data/mocked-user-data';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  users = UserList;
  private isAuthenticated: boolean;
  constructor() { }

  public isRouteAuthenticated(): boolean {
    return this.isAuthenticated;
  }

  public setIsAuthenticated(login: LoginModel): void {
    this.users.forEach(user => {
      if (user.email == login.email && user.password == login.password) {
        this.isAuthenticated = true;
      }
    })
  }
}
