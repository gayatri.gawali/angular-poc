import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExpandableGridComponent } from './component/expandable-grid/expandable-grid.component';
import { MatTableModule } from '@angular/material/table';
import { DynamicDetailRowComponent } from './component/dynamic-detail-row/dynamic-detail-row.component';
import { SimpleModalComponent } from './component/simple-modal/simple-modal.component';
import { FormsModule } from '@angular/forms';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { CdkDetailRowDirective } from './directive/cdk-detail-row.directive';

@NgModule({
  declarations: [ExpandableGridComponent, DynamicDetailRowComponent, CdkDetailRowDirective, SimpleModalComponent],
  imports: [
    CommonModule,
    MatTableModule,
    MatSlideToggleModule,
    FormsModule
  ],
  exports: [ExpandableGridComponent],
  entryComponents: [DynamicDetailRowComponent]
})
export class SharedModule { }
