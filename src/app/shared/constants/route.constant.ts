export class RouteConstant {
    static LOGIN = 'login';
    static DASHBOARD = 'dashboard';
    static TABLE = 'table';
    static EMPTY = '';
    static SLASH = '/'
}

