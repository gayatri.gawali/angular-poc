import { Component, OnInit, Input } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-dynamic-detail-row',
  templateUrl: './dynamic-detail-row.component.html',
  styleUrls: ['./dynamic-detail-row.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class DynamicDetailRowComponent implements OnInit {
  @Input() detailRowColumns: string[];
  @Input() expandedRow;
  @Input() data;
  constructor() { }

  ngOnInit() {
  }

}
