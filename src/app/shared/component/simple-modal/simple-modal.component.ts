import {Component} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';

/**
 * @title Dialog elements
 */
@Component({
  selector: 'app-simple-modal.component',
  templateUrl: 'simple-modal.component.html',
  styleUrls: ['simple-modal.component.scss'],
})
export class SimpleModalComponent {
  constructor(public dialog: MatDialog) {}

  openDialog() {
    this.dialog.open(SimpleModalComponent);
  }

  closeDialog() {
    this.dialog.closeAll();
  }
}
