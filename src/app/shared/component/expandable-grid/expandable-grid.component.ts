import { Component, Input, ViewChild, OnInit, ComponentFactoryResolver, ViewContainerRef, ViewChildren } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { DynamicDetailRowComponent } from '../dynamic-detail-row/dynamic-detail-row.component';

@Component({
  selector: 'app-expandable-grid',
  templateUrl: './expandable-grid.component.html',
  styleUrls: ['./expandable-grid.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ExpandableGridComponent implements OnInit {

  @Input() dataSource;  //= ELEMENT_DATA;
  @Input() columnsToDisplay: string[];
  @Input() detailRowColumns: string[];
  expandedRow: any // IPeriodicElement | null;
  // @ViewChild(DetailRowDirective, { static: false }) appDetailRow: DetailRowDirective;
  @ViewChild('tpl', { static: true, read: ViewContainerRef }) viewContainerRef: ViewContainerRef;

  constructor(private componentFactoryResolver: ComponentFactoryResolver) {
  }
  ngOnInit(): void {
  }
  loadComponent(element) {
    this.expandedRow = this.expandedRow === element ? null : element;
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(DynamicDetailRowComponent);
    // const viewContainerRef = this.appDetailRow.viewContainerRef;
    // viewContainerRef.clear();
    // if (this.expandedRow) {
    const componentRef = this.viewContainerRef.createComponent(componentFactory);
    (componentRef.instance as DynamicDetailRowComponent).data = [element];
    (componentRef.instance as DynamicDetailRowComponent).expandedRow = this.expandedRow;
    (componentRef.instance as DynamicDetailRowComponent).detailRowColumns = this.detailRowColumns;
    // }

  }

  isOpened(event, row) {
		row.opened = event;
	}
}


