import { ICustomer } from 'src/app/module/dashboard/customer.interface';

export const customerList: ICustomer[] =
  [
    {
      'id': 1,
      'first_name': 'Arluene',
      'last_name': 'Ricard',
      'email': 'aricard0@plala.or.jp',
      'gender': 'Female',
      status: true,
      billingAddress: 'Pune'
    },
    {
      'id': 984,
      'first_name': 'Clarke',
      'last_name': 'Nickols',
      'email': 'cnickolsrb@1688.com',
      'gender': 'Male',
      status: false,
      billingAddress: 'Pune'
    },
    {
      'id': 985,
      'first_name': 'Valentia',
      'last_name': 'Manktelow',
      'email': 'vmanktelowrc@accuweather.com',
      'gender': 'Female',
      status: false,
      billingAddress: 'Pune'
    },
    {
      'id': 986,
      'first_name': 'Ella',
      'last_name': 'Delort',
      'email': 'edelortrd@admin.ch',
      'gender': 'Female',
      status: false,
      billingAddress: 'Pune'
    },
    {
      'id': 987,
      'first_name': 'Lynett',
      'last_name': 'Grunnill',
      'email': 'lgrunnillre@google.pl',
      'gender': 'Female',
      status: false,
      billingAddress: 'Pune'
    },
    {
      'id': 988,
      'first_name': 'Hunt',
      'last_name': 'Cod',
      'email': 'hcodrf@woothemes.com',
      'gender': 'Male',
      status: true,
      billingAddress: 'Pune'
    },
    {
      'id': 989,
      'first_name': 'Lara',
      'last_name': 'Kubatsch',
      'email': 'lkubatschrg@ezinearticles.com',
      'gender': 'Female',
      status: true,
      billingAddress: 'Pune'
    },
    {
      'id': 990,
      'first_name': 'Felita',
      'last_name': 'Forkan',
      'email': 'fforkanrh@intel.com',
      'gender': 'Female',
      status: true,
      billingAddress: 'Pune'
    },
    {
      'id': 991,
      'first_name': 'Seumas',
      'last_name': 'Malenfant',
      'email': 'smalenfantri@merriam-webster.com',
      'gender': 'Male',
      status: true,
      billingAddress: 'Pune'
    },
    {
      'id': 992,
      'first_name': 'Lowe',
      'last_name': 'Peasee',
      'email': 'lpeaseerj@reddit.com',
      'gender': 'Male',
      status: true,
      billingAddress: 'Pune'
    },
    {
      'id': 993,
      'first_name': 'Geri',
      'last_name': 'Kalker',
      'email': 'gkalkerrk@plala.or.jp',
      'gender': 'Female',
      status: true,
      billingAddress: 'Pune'
    },
    {
      'id': 994,
      'first_name': 'Nerissa',
      'last_name': 'Wyer',
      'email': 'nwyerrl@gmpg.org',
      'gender': 'Female',
      status: true,
      billingAddress: 'Pune'
    },
    {
      'id': 995,
      'first_name': 'Giordano',
      'last_name': 'Antosch',
      'email': 'gantoschrm@ucsd.edu',
      'gender': 'Male',
      status: true,
      billingAddress: 'Pune'
    },
    {
      'id': 996,
      'first_name': 'Corabel',
      'last_name': 'O\'Dulchonta',
      'email': 'codulchontarn@hibu.com',
      'gender': 'Female',
      status: true,
      billingAddress: 'Pune'
    },
    {
      'id': 997,
      'first_name': 'Woody',
      'last_name': 'Deelay',
      'email': 'wdeelayro@cdbaby.com',
      'gender': 'Male',
      status: true,
      billingAddress: 'Pune'
    },
    {
      'id': 998,
      'first_name': 'Davita',
      'last_name': 'Standen',
      'email': 'dstandenrp@wikispaces.com',
      'gender': 'Female',
      status: true,
      billingAddress: 'Pune'
    },
    {
      'id': 999,
      'first_name': 'Aila',
      'last_name': 'Marchetti',
      'email': 'amarchettirq@vkontakte.ru',
      'gender': 'Female',
      status: true,
      billingAddress: 'Pune'
    },
    {
      'id': 1000,
      'first_name': 'Dorelle',
      'last_name': 'Winchcum',
      'email': 'dwinchcumrr@jimdo.com',
      'gender': 'Female',
      status: true,
      billingAddress: 'Pune'
    }
  ];