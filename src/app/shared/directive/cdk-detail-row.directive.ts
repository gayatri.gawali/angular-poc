import { Directive, HostBinding, HostListener, Input,
   TemplateRef, ViewContainerRef, EventEmitter, Output, ComponentFactoryResolver } from '@angular/core';
import { DynamicDetailRowComponent } from '../component/dynamic-detail-row/dynamic-detail-row.component';

@Directive({
  selector: '[cdkDetailRow]'
})
export class CdkDetailRowDirective {
  private row: any;
  private tRef: TemplateRef<any>;
  @Output() isOpened = new EventEmitter<boolean>();
  private opened: boolean;
  @HostBinding('class.expanded')
  get expended(): boolean {
    return this.opened;
  }


  @Input()
  set cdkDetailRow(value: any) {
    if (value !== this.row) {
      this.row = value;
      this.render(true);
    }
  }

  @Input('cdkDetailRowTpl')
  set template(value: TemplateRef<any>) {
    if (value !== this.tRef) {
      this.tRef = value;
      this.render(true);
    }
  }

  constructor(public vcRef: ViewContainerRef, private componentFactoryResolver: ComponentFactoryResolver) {

  }

  @HostListener('click')
  onClick(): void {
    this.toggle();
  }

  toggle(): void {
    if (this.opened) {
      this.vcRef.clear();
    } else {
      this.render(false);
    }
    this.opened = this.vcRef.length > 0;
    this.isOpened.emit(this.opened);
  }

  private render(isInitial: boolean): void {
    this.vcRef.clear();
    let showData = false;
    // condition to handle default open detail row
    if (this.tRef && this.row) {
      if (!isInitial) {
        showData = true;
        this.opened = false;
      }
      if (showData ) {
        const componentFactory = this.componentFactoryResolver.resolveComponentFactory(DynamicDetailRowComponent);
        const componentRef = this.vcRef.createComponent(componentFactory);
        (componentRef.instance as DynamicDetailRowComponent).data = this.row;
        // this.vcRef.createEmbeddedView(this.tRef, { $implicit: this.row });
        console.log('Data Rendered');
      }
    }
  }
}

