import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './module/login/login.component';
import { DashboardModule } from './module/dashboard/dashboard.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule, } from '@angular/forms';
import {
  MatDialogModule, MatCardModule, MatInputModule,
  MatIconModule, MatPaginatorModule, MatSelectModule, MatTableModule,
} from '@angular/material';
import { SimpleModalComponent } from './shared/component/simple-modal/simple-modal.component';
import { TextMaskModule } from 'angular2-text-mask';

export class MaterialModule { }

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    DashboardModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatCardModule,
    MatInputModule,
    TextMaskModule,
    MatButtonModule,
    MatTableModule,
  ],
  entryComponents: [SimpleModalComponent],
  providers: [],
  bootstrap: [AppComponent],

})
export class AppModule { }
