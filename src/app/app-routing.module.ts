import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './module/login/login.component';
import { RouteConstant } from './shared/constants/route.constant';
import { AuthGuardService } from './shared/core/auth-guard/auth-guard.service';
const routes: Routes = [
  { path: RouteConstant.EMPTY, redirectTo: RouteConstant.LOGIN, pathMatch: 'full' },
  { path: RouteConstant.LOGIN, component: LoginComponent },
  {
    path: RouteConstant.DASHBOARD,
    loadChildren: './module/dashboard/dashboard.module#DashboardModule',
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
